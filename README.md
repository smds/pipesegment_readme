The PipeSegment model mathematically represents a simple model of a straight pipe segment with a constant diameter, 
constant wall thickness, and optionally a constant thickness insulating jacket. This model takes
input and output boundary conditions in the form of a fluid flow message contract containing a temperture in Kelvin, 
a pressure in Pascals, and a mass flow rate in kilograms per second. Mass is assumed to be conserved in the pipe segment,
so the mass flow rate will be unchanged between the input and output. Heat loss is modeled through the walls of the pipe
segment assuming constant surface heat flux on the inside wall of the pipe segment. Pressure drop is modeled using the 
major loss correlation with a friction factor that is calculated based on the Reynolds number. The model is fully 
parametric and all physical parameters (length, diameter, etc.) can be specified by the system modeler at runtime.
The model is implemented in the C++ programming language and contains a single ordinary differential equation in the 
heat transfer portion of the model.